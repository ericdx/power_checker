
$dirs = @{}
$dirs.Add('srv1',@("\\net1\folder1"
))
$dirs.Add('srv2',@("\\net2\folder2","\\net2\folder3"))

$ext = @{}

$summary = 'CHECK PASSED'
$push = 0;
$now = Get-Date
foreach ($serv in $dirs.keys) {
foreach ($dir in $dirs[$serv]) {
Echo "$serv check"
$filter = 'bak'
if ($ext.contains($serv)) { $filter = $ext[$serv]}
 if ( [bool](Get-ChildItem -Filter "*.$filter" $dir |
  Where-Object { $_.LastWriteTime -gt $now.AddHours(-20) }) 
  ) { Echo " $dir fine" } else { Echo " $dir OLD !"; $summary = 'CHECK NOT PASSED'; $push = 1; };
  #Out-File 'i:\bkSeeker\checker.txt'
}
}
echo $summary
if ($push -eq 1) { & .\powerpush.ps1 bkSeek $summary ; }